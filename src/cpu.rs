use std::time::SystemTime;

use serde::{Deserialize, Serialize};
use sysinfo::{System, MINIMUM_CPU_UPDATE_INTERVAL};
use sysinfo::RefreshKind;
use sysinfo::CpuRefreshKind;

#[derive(Serialize, Deserialize, Clone)]
pub struct CPUUsage {
    pub ncores: u32,
    pub cores_usage: Vec<f64>,
    pub timestamp: u64,
}

pub fn get_cpu_usage() -> CPUUsage {
    let mut sys = System::new_with_specifics(
        RefreshKind::new().with_cpu(CpuRefreshKind::everything()),
    );
    let timestamp = SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .expect("Failed to obtain timestamp")
        .as_secs();

    std::thread::sleep(sysinfo::MINIMUM_CPU_UPDATE_INTERVAL);
    sys.refresh_cpu();

    let cores_usage: Vec<f32> = sys
        .cpus()
        .iter()
        .map(|processor| processor.cpu_usage())
        .collect();

    CPUUsage {
        ncores: sys.cpus().len() as u32,
        cores_usage: cores_usage.iter().map(|&x| x as f64).collect(),
        timestamp,
    }
}
