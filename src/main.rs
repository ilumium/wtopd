mod cpu;

use std::{env, io};

use futures_util::StreamExt;
use sysinfo::MINIMUM_CPU_UPDATE_INTERVAL;
//use log::info; // replaced by tracing
use tokio::net::{TcpListener, TcpStream};
use tracing::{error, info};

use futures_util::SinkExt;
use std::net::SocketAddr;
use tokio_tungstenite::{
    accept_async,
    tungstenite::{Error, Message, Result},
};

async fn handle_connection(
    peer: SocketAddr,
    stream: TcpStream,
    mut cpu_usage_rx: tokio::sync::watch::Receiver<cpu::CPUUsage>,
) -> Result<()> {
    let ws_stream = accept_async(stream).await.expect("Failed to accept");
    info!("New WebSocket connection: {}", peer);
    let (mut ws_sender, mut _ws_receiver) = ws_stream.split();

    loop {
        let cpu_usage = cpu_usage_rx.borrow_and_update().clone();
        if cpu_usage_rx.changed().await.is_err() {
            break;
        }
        ws_sender
            .send(Message::Text(serde_json::to_string(&cpu_usage).unwrap()))
            .await?;
    }
    Ok(())
}

async fn accept_connection(
    peer: SocketAddr,
    stream: TcpStream,
    cpu_usage_rx: tokio::sync::watch::Receiver<cpu::CPUUsage>,
) {
    if let Err(e) = handle_connection(peer, stream, cpu_usage_rx).await {
        match e {
            Error::ConnectionClosed | Error::Protocol(_) | Error::Utf8 => (),
            err => error!("Error processing connection: {}", err),
        }
    }
}

#[tokio::main]
async fn main() -> Result<(), io::Error> {
    tracing_subscriber::fmt::init();
    let addr = env::args()
        .nth(1)
        .unwrap_or_else(|| "127.0.0.1:8080".to_string());

    // create a watch channel for the CPU usage
    let (tx, rx) = tokio::sync::watch::channel(cpu::get_cpu_usage());

    // spawn a task to update the CPU usage
    tokio::spawn(async move {
        let mut interval = tokio::time::interval(MINIMUM_CPU_UPDATE_INTERVAL);
        loop {
            interval.tick().await;
            let cpu_usage = cpu::get_cpu_usage();
            tx.send(cpu_usage).unwrap();
        }
    });

    // Create the event loop and TCP listener we'll accept connections on.
    let try_socket = TcpListener::bind(&addr).await;
    let listener = try_socket.expect("Failed to bind");
    info!("Listening on: {}", addr);

    while let Ok((stream, _)) = listener.accept().await {
        let peer = stream.peer_addr().expect("Failed to obtain peer address");
        tokio::spawn(accept_connection(peer, stream, rx.clone()));
    }

    Ok(())
}
